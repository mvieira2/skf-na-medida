<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="utf-8">
	<meta http-equiv="cache-control" content="no-cache" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
	<title>SKF na Medida</title>
	<link href="<?php echo get_template_directory_uri(); ?>/layout/style.css?v=<?php echo get_rand(); ?>" rel="stylesheet" type="text/css">

	<script src="<?php echo get_template_directory_uri(); ?>/includes/angular/js/angular.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/angular/js/upload.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/jquery/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/notify/bootstrap-notify.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/js/bootstrap.min.js"></script>

</head>

<body class="login-body">
	<header class="skf-header">
		<div class="skf-logo"></div>
	</header>
