<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="utf-8">
	<meta http-equiv="cache-control" content="no-cache" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
	<title>SKF na Medida</title>

	<link href="<?php echo get_template_directory_uri(); ?>/layout/style.css?v=<?php echo get_rand(); ?>" rel="stylesheet" type="text/css">
	<script src="<?php echo get_template_directory_uri(); ?>/includes/angular/js/angular.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/angular/js/upload.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/jquery/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/notify/bootstrap-notify.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/js/bootstrap.min.js"></script>
	
	<script src="<?php echo get_template_directory_uri(); ?>/includes/slick/slick.min.js"></script>
	<link href="<?php echo get_template_directory_uri(); ?>/includes/slick/slick.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/includes/slick/slick-theme.min.css" rel="stylesheet" type="text/css">
	

</head>

<body ng-app="myApp">

	<header class="header" ng-controller="Header">
		<div id="loader" style="display: none;">
			<div class="fa-circle-o-notch fa-spin"></div>
		</div>
		<div class="header-limit clearfix">
			<a href="<?php echo home_url(); ?>"><div class="logo"></div></a>
			<a href="<?php echo home_url('login'); ?>"> <div class="header-icon signout" title="Sair"></div></a>
			
			<div class="header-icon support" title="Enviar mensagem para o suporte"></div>
			<label class="header-icon notification" title="Notificações recebidas" for="notifications" ng-class="{'exist-notification' : notifications_not_read.length>0}" data-value="{{ notifications_not_read.length }}"></label>
			<div class="header-icon user" title="Perfil">
			<!--<figure class="avatar">
				<img src="images/sample.png" />
			</figure>-->
		</div>
		<input type="checkbox" id="notifications" class="hidden-input" />
		<div class="notification-wrapper">
			<div class="header-notification">
				<span class="notification-title">Notificações</span>
				<ul>
					<li ng-repeat="notification in notifications track by notification.ID " ng-class="{'is-read' : notification.is_read==1}">
						<a ng-href="{{notification.link}}" ng-click="read_notify(notification.ID)">
							
							<div class="notification-picture" >
								<figure class="" ng-class="{'notification-icon icon-bell' : !notification.image_notify, 'avatar' : notification.image_notify}">
									<img ng-if="notification.image_notify" ng-src="{{ notification.image_notify }}" />
								</figure>
							</div>
							<div class="notification-content">
								<div class="notification-preview"><strong>{{ notification.name || 'Notificação'}}</strong> {{ notification.content }}</div>
								<div class="notification-info" ng-show="notification.date_created">{{ notification.date_created }}</div>
							</div>
						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
</header>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/App.js?v=<?php echo get_rand(); ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/HeaderController.js?v=<?php echo get_rand(); ?>"></script>