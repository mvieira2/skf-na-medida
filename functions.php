<?php

// echo "<pre>";
//  print_r( wp_get_attachment_image_src(44)[0] );
// echo "</pre>";

require_once 'api/endpoints.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

add_action( 'delete_attachment', 'delete_attachment_fc', 10 );
function delete_attachment_fc( $id ){


	global $wpdb;
	$wpdb->delete( "{$wpdb->prefix}face_posts_images", array( 'post_id' => $id) );


}



function paulund_remove_default_image_sizes( $sizes) {
	unset( $sizes['600x600']);
	unset( $sizes['medium']);
	unset( $sizes['medium_large']);
	unset( $sizes['large']);

	return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'paulund_remove_default_image_sizes');



add_filter('wp_handle_upload', 'max_dims_for_new_uploads', 10, 2 );
function max_dims_for_new_uploads( $array, $context ) {
	$ok = array( 'image/jpeg', 'image/gif', 'image/png' );
	if ( ! in_array( $array['type'], $ok ) ) return $array;
	$editor = wp_get_image_editor( $array['file'] );
	if ( is_wp_error( $editor ) )
		return $editor;
	$editor->set_quality( 80 );
	$editor->resize( 600, 600 ); 
	$editor->save( $array['file'] );
	return $array;
}

add_filter( 'authenticate', 'myplugin_auth_signon', 30, 3 );
function myplugin_auth_signon( $user, $username, $password ) {

	if (property_exists($user, 'ID')) {
		$current_user = get_user_by('ID',$user->ID); 

	if ($user && $current_user->first_name) { // alterando formato nome
		$user->data->display_name = $current_user->first_name.' '.$current_user->last_name;
	}
}

	//echo "<pre>";
	//print_r($user);
	//echo "</pre>";

return $user;
}




function get_rand()
{   
	return 'v3'.rand();
	//return wp_get_theme()->get( 'Version' );
}


