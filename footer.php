<?php //wp_footer(); ?>

<footer class="footer">
	<div class="footer-limit">
		<ul>
			<li><a href="#">Suporte</a></li>
			<li><a href="#">Regulamento</a></li>
			<li><a href="#">Termos de uso</a></li>
			<li><a href="#">Política de Privacidade</a></li>
		</ul>
		<span>SKF na Medida © SKF do Brasil</span>
	</div>
	<div class="skf-footer">
		<div class="skf-logo"></div>
	</div>
</footer>


<script type="text/javascript">

	var 	edit_txt = $('.edit-post-textarea'),
	edit_hiddenDiv = $(document.createElement('div')),
	edit_content = null;

	edit_hiddenDiv.addClass('edit-post-div-textarea');

	$('body').append(edit_hiddenDiv);

	edit_txt.on('keyup', function () {
		edit_content = $(this).val();
		edit_content = edit_content.replace(/\n/g, '<br>');
		edit_hiddenDiv.html(edit_content + '<br>');
		$(this).css('height', edit_hiddenDiv.height());

	});

	var 	txt = $('.comments-textarea'),
	hiddenDiv = $(document.createElement('div')),
	content = null;

	hiddenDiv.addClass('comments-div-textarea');

	$('body').append(hiddenDiv);

	txt.on('keyup', function () {
		content = $(this).val();
		content = content.replace(/\n/g, '<br>');
		hiddenDiv.html(content + '<br>');
		$(this).css('height', hiddenDiv.height());

	});

</script>
</body>
</html>