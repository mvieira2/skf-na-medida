<?php
require_once __DIR__ . '/Autoload.php';


add_action( 'rest_api_init', function ( $server ) {

	register_rest_route( 'api/v1', '/teste1', array(
		'methods'  => 'GET',
		//'args' => array(),		
		//'permission_callback' => function () { return current_user_can( 'administrator' ); } , 
		'callback' => 'teste1',
	));	
	

	$notification = new Notifications;
	register_rest_route( 'api/v1', '/notifications', array(
		'methods'  => 'POST',
		'args' => $notification->args(),		
		// 'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $notification, 'notifications' )
	));	
	
	register_rest_route( 'api/v1', '/read_notification', array(
		'methods'  => 'POST',
		'args' => $notification->args2(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $notification, 'read_notification' )
	));	

	$image = new Image;
	register_rest_route( 'api/v1', '/image', array(
		'methods'  => 'GET',
		'args' => $image->args(),		
		//'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $image, 'image' )
	));	
	
	register_rest_route( 'api/v1', '/upload_image', array(
		'methods'  => 'POST',
		//'args' => $image->args2(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $image, 'upload_image' )
	));		
	register_rest_route( 'api/v1', 'delete_image', array(
		'methods'  => 'POST',
		'args' => $image->delete_image_args(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $image, 'delete_image' )
	));	
	
	register_rest_route( 'api/v1', '/images_sem_post', array(
		'methods'  => 'POST',
		//'args' => $image->args2(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $image, 'images_sem_post' )
	));	

	$post = new Posts;
	register_rest_route( 'api/v1', '/posts', array(
		'methods'  => 'POST',
		'args' => $post->args(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $post, 'posts_list' )
	));	

	register_rest_route( 'api/v1', '/new_post', array(
		'methods'  => 'POST',
		'args' => $post->args2(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $post, 'new_post' )
	));	

	register_rest_route( 'api/v1', '/new_comments', array(
		'methods'  => 'POST',
		'args' => $post->args3(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $post, 'new_comments' )
	));	
	
	register_rest_route( 'api/v1', '/delete_comments', array(
		'methods'  => 'POST',
		'args' => $post->args4(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $post, 'delete_comments' )
	));		

	register_rest_route( 'api/v1', '/update_comments', array(
		'methods'  => 'POST',
		'args' => $post->args5(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $post, 'update_comments' )
	));	
	
	register_rest_route( 'api/v1', '/like_post', array(
		'methods'  => 'POST',
		'args' => $post->args6(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $post, 'like_post' )
	));	
	
	register_rest_route( 'api/v1', '/like_comment', array(
		'methods'  => 'POST',
		'args' => $post->args6(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $post, 'like_comment' )
	));	

	$tags = new Tags;
	register_rest_route( 'api/v1', '/tags', array(
		'methods'  => 'GET',
		'args' => $tags->args(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $tags, 'getTags' )
	));	

	$user = new User;
	register_rest_route( 'api/v1', '/current_user', array(
		'methods'  => 'POST',
		//'args' => $user->args(),		
		'permission_callback' => function () { return current_user_can( 'administrator' ) || current_user_can( 'subscriber' ); } , 
		'callback' => array( $user, 'getCurrentUser' )
	));	

});


function teste1( WP_REST_Request $request ) 
{
	$img = get_template_directory_uri() . '/layout/images/sample.png';
	//$img = 'https://placehold.it/350x500&text=Ola%20teste%20imagem';

	 //$im = imagecreatefrompng( $img);


	// $im = imageresolution($im, 300, 72);
	 //header('Content-Type: image/png');

	// imagepng($im , null, 9 );
	// imagedestroy($im);
	$image = new Image;
	imagejpeg($image->resize_image($img, $request->get_param("width"), $request->get_param("height")), null, 60);

	//return "teste 1 endpoint";
}
