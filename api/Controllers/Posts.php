<?php

class Posts
{
	
	public function args()
	{
		return array();
	}


	public function posts_list($request)
	{
		global $wpdb;
		$userID = get_current_user_id();
		$path = content_url('uploads/');  

		$total_reg = 10;
		$pagina= $request->get_param("page");
		if (!$pagina) {
			$pc = 1;
		} else {
			$pc = $pagina;
		}

		$inicio = $pc - 1;
		$inicio = $inicio * $total_reg;

		$query = "
		SELECT SQL_CALC_FOUND_ROWS
		POST.*
		, USER.display_name as user_name  
		, ( SELECT COUNT(*) FROM `{$wpdb->prefix}face_posts_likes` as L WHERE L.ID_face_post = POST.ID  ) as count_likes
		, ( SELECT COUNT(*) FROM `{$wpdb->prefix}face_posts_likes` as L WHERE L.ID_face_post = POST.ID AND L.ID_user = {$userID} ) as 'liked'
		, ( SELECT COUNT(C.ID) FROM `{$wpdb->prefix}face_posts_comments` as C WHERE C.ID_face_post = POST.ID  ) as count_comments
		FROM `{$wpdb->prefix}face_posts` as POST
		INNER JOIN `{$wpdb->prefix}users` as USER
		ON USER.ID = POST.ID_user
		ORDER BY POST.date_created desc
		LIMIT $inicio,$total_reg; ";

		$results = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );
		$total_page = ceil($wpdb->get_var('SELECT FOUND_ROWS()') / $total_reg);

		foreach ($results as $key => $result) {

			$query2 = "
			SELECT 
			COMMENT.*
			, USER.display_name as user_name 
			, ( SELECT COUNT(*) FROM `{$wpdb->prefix}face_posts_comments_likes` as L WHERE L.ID_face_post_comment = COMMENT.ID  ) as count_likes
			, ( SELECT COUNT(*) FROM `{$wpdb->prefix}face_posts_comments_likes` as L WHERE L.ID_face_post_comment = COMMENT.ID AND L.ID_user = {$userID} ) as 'liked'
			FROM `{$wpdb->prefix}face_posts_comments` as COMMENT
			INNER JOIN `{$wpdb->prefix}users` as USER
			ON USER.ID = COMMENT.ID_user
			WHERE COMMENT.ID_face_post = {$result->ID}
			ORDER BY COMMENT.ID asc
			";


			$result->comments = $wpdb->get_results( $wpdb->prepare($query2, OBJECT ) );


			$query_images = "
			SELECT 
			IMAGE.ID 
			, CONCAT('{$path}', WP_POST_META.meta_value) as 'image'
			FROM wp_face_posts_images  as IMAGE
			INNER JOIN wp_posts as WP_POST
			ON WP_POST.ID = IMAGE.post_id AND WP_POST.post_type = 'attachment'
			INNER JOIN wp_postmeta as WP_POST_META
			ON WP_POST_META.post_id = WP_POST.ID AND WP_POST_META.meta_key = '_wp_attached_file'
			WHERE IMAGE.ID_face_post = {$result->ID}
			";

			$result->images = $wpdb->get_results( $wpdb->prepare($query_images, OBJECT ) );


			$query_tags = "			
			SELECT TAG.ID, TAG.name FROM {$wpdb->prefix}face_posts_tags as TAG_RELATION
			INNER JOIN {$wpdb->prefix}face_tags as TAG
			ON TAG_RELATION.ID_face_tag = TAG.ID
			WHERE ID_face_post = {$result->ID} 
			";


			$result->tags = $wpdb->get_results( $wpdb->prepare($query_tags, OBJECT ) );

			foreach ($result->comments as $key => $comment) {
				$comment->date_created = human_time_diff( strtotime( $comment->date_created ), current_time( 'timestamp' ) ).' '.__( 'atrás' );
			}

			$result->date_created = human_time_diff( strtotime( $result->date_created ), current_time( 'timestamp' ) ).' '.__( 'atrás' );
		}

		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $results, 'page' => $pc , 'total' => $total_page );
	}

	public function args2()
	{
		return array(
			'content' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_string( $param );
				}
			),
			'tags' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_array( $param );
				}
			),
			'images' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_array( $param );
				}
			),
		);
	}

	public function new_post($request)
	{
		global $wpdb;
		$userID = get_current_user_id();

		$data = array(
			'ID_user' => $userID,
			'content' => $request->get_param('content')  
		);

		$post = $wpdb->insert("{$wpdb->prefix}face_posts",$data,array('%s'));
		$postID = $wpdb->insert_id;
		
		$results = [];

		if ($request->offsetExists('images') && $postID) {
			
			foreach ($request->get_param('images') as $key => $image) {

				$dataImage = array('ID_face_post' => $postID );
				$whereImage = array(
					'ID_user' =>  $userID,
					'post_id' =>  (int) $image['ID'] 
				);
				$results[] = $wpdb->update("{$wpdb->prefix}face_posts_images", $dataImage, $whereImage);

			}

		}
		

		if ($request->offsetExists('tags') && $postID) {
			
			foreach ($request->get_param('tags') as $key => $tag) {

				$dataTag = array('ID_face_post' => $postID, 'ID_face_tag' => $tag['ID'] );
				$wpdb->insert("{$wpdb->prefix}face_posts_tags", $dataTag);

				$results[] = $wpdb->last_query;

			}

		}


		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $results );



	}

	public function args3()
	{
		return array(
			'IDpost' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'comment' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_string( $param );
				}
			),
		);
	}


	public function new_comments($request)
	{
		global $wpdb;
		$userID = get_current_user_id();

		$data = array(
			'ID_face_post' => $request->get_param("IDpost"), 
			'ID_user' => $userID,
			'content' => $request->get_param("comment"),
		);

		$results = $wpdb->insert("{$wpdb->prefix}face_posts_comments", $data);

		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $results );

	}

	public function args4()
	{
		return array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			)
		);
	}

	public function delete_comments($request)
	{
		global $wpdb;
		$userID = get_current_user_id();

		$where = array(
			'ID' => $request->get_param("ID"),
			'ID_user' => $userID,
		);
		
		$results = $wpdb->delete("{$wpdb->prefix}face_posts_comments", $where);


		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		$where2 = array(
			'ID_face_post_comment' => $request->get_param("ID"),
			'ID_user' => $userID,
		);
		
		$wpdb->delete("{$wpdb->prefix}face_posts_comments_likes", $where2);

		return array('error' => false, 'data' => $results );
	}

	public function args5()
	{
		return array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'content' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_string( $param );
				}
			),
		);
	}

	public function update_comments($request)
	{
		global $wpdb;
		$userID = get_current_user_id();

		$data = array(
			'content' => $request->get_param("content"),
			'date_created' => current_time( 'mysql' )
		);

		$where = array(
			'ID' => $request->get_param("ID"),
			'ID_user' => $userID,
		);
		
		$results = $wpdb->update("{$wpdb->prefix}face_posts_comments", $data ,$where);


		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $results );
	}


	public function args6()
	{
		return array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			)
		);
	}

	public function like_post($request)
	{
		global $wpdb;
		$userID = get_current_user_id();
		$ID = $request->get_param("ID");

		$results = [];

		$query = "
		SELECT * FROM {$wpdb->prefix}face_posts_likes
		WHERE ID_user = {$userID} AND ID_face_post = {$ID} AND 1 ;
		";


		$results1 = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );



		if ($wpdb->num_rows > 0) {
			
			$where1 = array('ID_face_post' => $ID, 'ID_user' => $userID );
			$results[] = $wpdb->delete("{$wpdb->prefix}face_posts_likes", $where1);

		}

		else {

			$where1 = array('ID_face_post' => $ID, 'ID_user' => $userID );
			$results[] = $wpdb->insert("{$wpdb->prefix}face_posts_likes", $where1);

		}


		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $results );
	}

	public function like_comment($request)
	{
		global $wpdb;
		$userID = get_current_user_id();
		$ID = $request->get_param("ID");

		$results = [];

		$query = "
		SELECT * FROM {$wpdb->prefix}face_posts_comments_likes
		WHERE ID_user = {$userID} AND ID_face_post_comment = {$ID} AND 1 ;
		";

		$results1 = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );

		if ($wpdb->num_rows > 0) {
			$where1 = array('ID_face_post_comment' => $ID, 'ID_user' => $userID );
			$results[] = $wpdb->delete("{$wpdb->prefix}face_posts_comments_likes", $where1);
		}

		else {
			$where1 = array('ID_face_post_comment' => $ID, 'ID_user' => $userID );
			$results[] = $wpdb->insert("{$wpdb->prefix}face_posts_comments_likes", $where1);
		}

		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $results );
	}

}