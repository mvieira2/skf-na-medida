<?php 

/**
 * 
 */
class Image 
{
	
	function __construct()
	{
		# code...
	}


	public function resize_image($file, $w, $h, $crop=false)
	{


		list($width, $height) = getimagesize($file);
		$r = $width / $height;
		if ($crop) {
			if ($width > $height) {
				$width = ceil($width-($width*abs($r-$w/$h)));
			} else {
				$height = ceil($height-($height*abs($r-$w/$h)));
			}
			$newwidth = $w;
			$newheight = $h;
		} else {
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
		}

		$exploding = explode(".",$file);
		$ext = end($exploding);

		switch($ext){
			case "png":
			$src = imagecreatefrompng($file);
			$type = "Content-Type: image/png";
			break;
			case "jpeg":
			case "jpg":
			$src = imagecreatefromjpeg($file);
			$type = "Content-Type: image/jpg";
			break;
			case "gif":
			$src = imagecreatefromgif($file);
			$type = "Content-Type: image/gif";
			break;
			default:
			$src = imagecreatefromjpeg($file);
			$type = "";
			break;
		}

		$dst = imagecreatetruecolor($newwidth, $newheight);
		$tt = imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		if (!$tt) {
			return false;
		}

		//header($type);

		return $dst;

	}

	public function args()
	{
		return array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'width' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
			'height' => array(
				'required' => false,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
		);
	}

	public function image($request)
	{
		//$targetdir = content_url('uploads/');   

		//$img = get_template_directory_uri() . '/layout/images/no_image.png';
		//$img = content_url('all_images') . "/{$request->get_param('ID')}.png";

		$img = wp_get_attachment_image_src(44)[0];

		return $img;

		$width = $request->get_param("width") ? $request->get_param("width") : 400;
		$height = $request->get_param("height")? $request->get_param("height") : 400;


		$file = $this->resize_image($img, $width, $height);

		if ($file !== false) {
			return imagejpeg($file, null, 60);
		}



		return new WP_Error( 'no_image', "No image exist {$img}", array( 'status' => 500 ) );

	}

	public function args2() {
		return array();
	}

	public function upload_image($request){

		global $wpdb;
		$userID = get_current_user_id();

		$params = $request->get_params();

		$files   = $request->get_file_params();
		$headers = $request->get_headers();

		if (count($files["file"]["name"]) > 10) {
			return new WP_Error( 'max_images', "Você só pode subir até 10 imagens.", array( 'status' => 500 ) );
		}

		if ( ! empty( $files ) ) {
			$ids_posts = $this->upload_from_file( $files );
		} 


		if ( is_wp_error( $ids_posts ) ) {
			return $ids_posts;
		}

		foreach ($ids_posts as $key => $id) {
			$data = array('ID_user'  => $userID, 'post_id' => $id );

			$wpdb->insert("{$wpdb->prefix}face_posts_images",$data,array('%d','%d'));
		}

		return $this->images_sem_post();
		
	}

	public function images_sem_post($request='')
	{
		global $wpdb;
		$userID = get_current_user_id();
		$path = content_url('uploads/');  

		$query = "

		SELECT IMAGE.post_id as 'ID'
		, CONCAT('{$path}', WP_POST_META.meta_value) as 'image'
		FROM {$wpdb->prefix}face_posts_images  as IMAGE
		INNER JOIN {$wpdb->prefix}posts as WP_POST
		ON WP_POST.ID = IMAGE.post_id AND WP_POST.post_type = 'attachment'
		INNER JOIN {$wpdb->prefix}postmeta as WP_POST_META
		ON WP_POST_META.post_id = WP_POST.ID AND WP_POST_META.meta_key = '_wp_attached_file'

		WHERE IMAGE.ID_user = {$userID} AND IMAGE.ID_face_post IS NULL
		";

		$results = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );

		//print_r($wpdb->last_query);

		return array('error' => false, 'data' => $results );
	}


	protected function upload_from_file( $img ) {

		$ids_posts = [];

		if(!empty($img))
		{
			$img_desc = $this->reArrayFiles($img['file']);

			if ( ! function_exists( 'wp_handle_upload' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}

			require_once( ABSPATH."wp-admin/includes/image.php" );

			$upload_overrides = array( 'test_form' => false );

			foreach($img_desc as $val)
			{

				$uploaded_file = wp_handle_upload( $val, $upload_overrides );

				if( isset( $uploaded_file  )) {
					$file_name_and_location = $uploaded_file['file'];
					$file_title_for_media_library = $val['name'];

					$attachment = array(
						"post_mime_type" => $val['type'],
						"post_title" => preg_replace( '/\.[^.]+$/', '', basename( $file_title_for_media_library ) ),
						"post_content" => "",
						"post_status" => "inherit",
					);

					$id = wp_insert_attachment( $attachment, $file_name_and_location );
					$ids_posts[] = $id;
					//set_post_thumbnail( 1, $id );


					$attach_data = wp_generate_attachment_metadata( $id, $file_name_and_location );
					wp_update_attachment_metadata( $id, $attach_data );

				} 
			}

		}
		return $ids_posts;

	}

	protected function reArrayFiles($file)
	{
		$file_ary = array();
		$file_count = count($file['name']);
		$file_key = array_keys($file);

		for($i=0;$i<$file_count;$i++)
		{
			foreach($file_key as $val)
			{
				$file_ary[$i][$val] = $file[$val][$i];
			}
		}
		return $file_ary;
	}	

	public function delete_image_args()
	{
		return array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
		);
	}

	public function delete_image($request)
	{

		global $wpdb;
		$userID = get_current_user_id();
		$ID_image = $request->get_param("ID");

		$query = "SELECT `ID_user`
		FROM {$wpdb->prefix}face_posts_images WHERE {$ID_image} = `post_id` AND `ID_user` = {$userID}  AND 1;
		";

		$ID_User_query = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );

		if ( $wpdb->num_rows > 0 ) {
			

			$results = wp_delete_attachment( $ID_image );

			if (false === $results) {
				return new WP_Error( 'no_delete', "No deleted 1", array( 'status' => 500 ) );
			}

			return array('error' => false, 'data' => $results );

		}

		return new WP_Error( 'no_delete', "No deleted 2", array( 'status' => 500 ) );
	}

}

?>

