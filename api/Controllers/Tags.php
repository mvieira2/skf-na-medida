<?php 

/**
 * Tags
 */
class Tags
{
	
	public function args()
	{
		return array();
	}


	public function getTags($request)
	{
		global $wpdb;


		$query = "
		SELECT * FROM {$wpdb->prefix}face_tags_category WHERE 1;
		";

		$results = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );

		foreach ($results as $key => $result) {

			$query2 = "
				SELECT * FROM {$wpdb->prefix}face_tags WHERE category_tag_ID = {$result->ID};
			";

			$result->tags = $wpdb->get_results( $wpdb->prepare($query2, OBJECT ) );

		}

		if ( false === $results ) {
			return new WP_Error( 'query', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $results );


	}


}



?>