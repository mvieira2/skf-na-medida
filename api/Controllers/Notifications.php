<?php

class Notifications
{
	
	function __construct()
	{
		# code...
	}

	public function args()
	{
		return array();
	}

	public function notifications($request)
	{
		global $wpdb;
		$userID = get_current_user_id();

		$query = "SELECT * FROM `{$wpdb->prefix}face_notifications`  WHERE `ID_user` = $userID OR `ID_user` IS NULL;";

		$results = $wpdb->get_results( $wpdb->prepare($query, OBJECT ) );

		array_unshift( 
			$results,
			array('content' => 'Notificacao estatica PHP', 'is_read' => 0,'link' => "?linkTeste")
		);


		foreach ($results as $key => $result) {
			//$result->image_notify = get_template_directory_uri(). '/layout/images/sample.png';
			$result->date_created = human_time_diff( strtotime( $result->date_created ), current_time( 'timestamp' ) ).' '.__( 'atrás' );
		}


		return array('error' => false, 'data' => $results );
	}

	public function args2()
	{
		return array(
			'ID' => array(
				'required' => true,
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
		);
	}

	public function read_notification($request)
	{
		global $wpdb;

		$data = array('is_read' => 1);
		$where = array('ID' => $request->get_param('ID'), 'is_read' => 0);

		$result = $wpdb->update( "{$wpdb->prefix}face_notifications", $data, $where );
		
		
		if ( false === $result ) {
			return new WP_Error( 'no_insert_or_update', $wpdb->last_error, array( 'status' => 400 ) );
		} 

		return array('error' => false, 'data' => $result );
	}
}