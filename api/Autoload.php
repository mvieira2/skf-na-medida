<?php


spl_autoload_register( function( $class ) {

    $dir = __DIR__.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR;

    foreach ( scandir( $dir ) as $file ) {

      if ( is_dir( $dir.$file ) && substr( $file, 0, 1 ) !== '.' )
        autoload( $class, $dir.$file.'/' );

    if ( substr( $file, 0, 2 ) !== '._' && preg_match( "/.php$/i" , $file ) ) {

        if ( str_replace( '.php', '', $file ) == $class || str_replace( '.class.php', '', $file ) == $class ) {

            include $dir . $file;
        }
    }
}
});

