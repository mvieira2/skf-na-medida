app.controller('Home', function($scope, $rootScope, $location,$timeout, API, Upload, $q) {

	$scope.images_statics = [];
	$scope.posts = [];
	$scope.newPostModel = { 'images' : [], 'tags' : [], 'content' : '' };
	$scope.newPost = angular.copy( $scope.newPostModel );

	$scope.init = function () {

		var promise1 = API.get("tags").then(function (r0) {
			$scope.tags = r0.data;
		});

		var promise2 = API.post("current_user").then(function (r1) {

			$scope.current_user = r1.data;

			$scope.loadPosts();
		});

		return $q.all([promise1, promise2])
	}

	$scope.loadPosts = function (pageI) {

		var data = {};
		$scope.postsPagination = {};

		if (pageI) {
			data["page"] = pageI;

		}
		
		API.post("posts", data).then(function (r) {
			if (!r.error) {

				if (!!!r.data['status']) {

					if ($scope.posts.length && pageI) {
						$scope.posts = $scope.posts.concat(r.data);
					}
					else {
						$scope.posts = r.data;
					}

					$scope.postsPagination = { 'total' : r.total, 'page' : r.page  };

					$timeout(function () {

						$('.slider-1').not(".iniciado").addClass("iniciado").slick({
							dots: true,
							infinite: true,
							speed: 300,
							slidesToShow: 1,
							slidesToScroll: 1,

						});

					},500);

					$scope.get_images_sem_post();

				}
			}
		});
	}

	$scope.get_images_sem_post = function (argument) {
		API.post("images_sem_post").then(function (resp) {
			if (!!!resp.data['status']) {
				$scope.images_statics = resp.data;

				if ($scope.newPost.images.length) {
					$scope.newPost.images = angular.copy($scope.images_statics);

				}

			}

		})
	}
	
	$scope.uploadFileOk = function(){
		$scope.newPost.images = angular.copy($scope.images_statics);
		$(".edit-post-textarea").focus();
		$("#modal-picture").prop('checked', false);
	};

	$scope.addTagToNewPost = function(){

		$scope.newPost.tags = [];

		$scope.tags.map(function (category) {
			return category.tags.filter(function (tag) {
				return tag.selected;
			});
		}).map(function (cat) {
			return cat.map(function (cat1) {
				return { 'ID' : cat1.ID, 'name' : cat1.name };
			})
		}).map(function (cat) {
			if (cat.length) {
				$scope.newPost.tags = $scope.newPost.tags.concat(cat);
				return cat;
			}

		})

		$(".edit-post-textarea").focus();
		$("#modal-tag").prop('checked', false);

	}

	$scope.carregarImagens =  function (arquivos) {

		var files = arquivos[0].files;
		var filesBase64 = [];

		Upload.upload({
			url:  urlHome.replace("/login/","/")+ "wp-json/api/v1/upload_image",
			data: {file: files, user_email: oAuth.user_email },
			headers: {
				Authorization: 'Bearer '+oAuth.token,
			}
		}).then(function (resp) {
			
			if (!!!resp.data['data']['status']) {
				$scope.images_statics = resp.data.data;
			}
			$scope.msgUpload = null;

		}, function (resp) {
			$scope.msgUpload = null;
			console.log('Error status: ' + resp.status);

			if (resp.data.code && resp.data.code == "max_images") {
				API.alertar_erro(resp.data.message);
			}


		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);

			$scope.msgUpload = 'Subindo as imagens: ' + progressPercentage + '% ';
		});		
	}

	$scope.removeImage = function (ID) {
		if (confirm("Deseja excluir essa imagem?")) {
			API.post("delete_image", { ID:ID}).then(function (r) {
				//API.alertar_sucesso(r.data);

				$scope.get_images_sem_post();
			})
		}
	}

	$scope.sendNewPost = function (newPost) {

		var newPostCopy = angular.copy(newPost);
		$scope.newPost = angular.copy( $scope.newPostModel );

		API.post("new_post", newPostCopy).then(function (r) {

			console.log(r);

			$scope.init();
		})
	}

	$scope.sendComments = function (post) {
		
		post.loadedComments = true;
		post.loadedComments1 = true;
		var comments = angular.copy( post.new_comments );
		post.new_comments = "";

		API.post("new_comments", { IDpost : post.ID, comment : comments }).then(function (r) {
			
			console.log(r);

			comment1 = "";

			$scope.init().then(function (r) {
				post.loadedComments = false;
				post.loadedComments1 = false;
			}).reject(function (r) {
				post.loadedComments = false;
				post.loadedComments1 = false;
			});


		})


	}

	$scope.deleteComments = function (comment1, post) {
		if (confirm("Deseja realmente excluir esse comentário?")) {

			var comment = angular.copy(comment1);
			post.loadedComments1 = true;


			API.post("delete_comments", { ID : comment.ID }).then(function (r) {

				console.log(r);

				$scope.init().then(function (r) {
					post.loadedComments1 = false;
				}).reject(function (r) {
					post.loadedComments1 = false;
				});

			})
		}
	}


	$scope.focusComments = function (ID) {
		$timeout(function () {
			$("#"+ID).focus();
		}, 500)
	}

	$scope.editCommentNow = function (comment) {
		comment.isEdit=true;
		$timeout(function () {
			$("#comment_field_"+comment.ID).focus();
		}, 350)
		console.log("#comment_field_"+comment.ID)
	}

	$scope.like_post = function (post) {
		post.liked = !Number(post.liked);

		API.post("like_post", { ID : post.ID }).then(function (r) {
			$scope.init();

		})
	}

	$scope.like_comment = function (comment) {
		comment.liked = !Number(comment.liked);

		API.post("like_comment", { ID : comment.ID }).then(function (r) {
			$scope.init();

		})
	}

	$scope.updateComments = function (comment) {
		comment.isEdit=false;
		API.post("update_comments", comment).then(function (r) {

			$scope.init();

		});
	}

	$scope.init();

});


$(document).on("click","img", function (e) {


	if ($(this).hasClass("img-view")) {
		var url = $(this).attr("src");
		$(".modal-zoom-media img").attr("src", url);
	}


})