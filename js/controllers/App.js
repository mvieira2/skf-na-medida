var app = angular.module('myApp', ['ngFileUpload']);
var headerApp = angular.module('headerApp', []);
var isDev = window.location.href.indexOf("localhost") > -1;
var urlHome = isDev ? window.location.protocol+"//localhost:83/freelas/skf-na-medida/login/" : window.location.protocol+'//competencemapping.rotativoskf.com.br/homolog/';
var urlAPI = urlHome.replace("/login/","/") + "wp-json/api/v1/";

var oAuth;

try {
	oAuth = JSON.parse(localStorage.getItem('oAuth@namedida'));

} catch(e) {
	localStorage.removeItem('oAuth@namedida');

	oAuth = {};
};
if (oAuth && oAuth.user_display_name) {
	setTimeout(function (argument) {
		$("#userName").html( oAuth.user_display_name ).attr("title", oAuth.user_display_name);
	},500)
};
if (oAuth && !oAuth.token ||  location.origin + location.pathname != urlHome && oAuth === null) {
	var param = "?redirect=" +window.location.href;

	window.location.href = urlHome + param;

};

app.factory("API", function($http, $rootScope){
	var api = {
		urlApiBase : urlHome.replace("/login/","/") + "wp-json/",
		urlAPI : urlHome.replace("/login/","/")+ "wp-json/api/v1/",
		loading : function (show) {
			
			if (show) $("#loader").fadeIn('slow');

			if (!show) {
				clearTimeout(debounceAJAX);
				var debounceAJAX = setTimeout(function(){
					$("#loader").fadeOut();
				}, 1000);
			}
		},
		alertar_sucesso : function (msg) {
			alert(msg);
		},
		alertar_erro :  function (msg) {
			alert(msg);
		},
		get_params_url : function (param, url) {

			function isIE(userAgent) {
				userAgent = userAgent || navigator.userAgent;
				return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1;
			}
			if (!isIE()) {
				var url = new URL(url||window.location.href);
				return url.searchParams.get(param)
			}
		},
		call : function (chamada, metodo,parametros, callback ) {
			let self = this;
			self.loading(1);


			return $http({
				url: self.urlAPI + chamada,
				method: (metodo||'POST'),
				data: (parametros||null),
				headers: {
					Authorization: 'Bearer '+oAuth.token,
				},
				onProgress: function (e) {
					if (e.lengthComputable) {
						$rootScope.progressBar = (e.loaded / e.total) * 100;
						$rootScope.progress = $rootScope.progressBar;
					}
				}
			}).then(function (r) {
				if (typeof callback === 'function') {
					callback(r.data);
				}
				self.loading(0);
				return r.data;

			}, function (error) {
				if (typeof callback === 'function') {
					callback(error.data);
				}
				self.loading(0);

				if (error.data && error.data.hasOwnProperty('message')) {

					if (error.data['code'] == 'jwt_auth_invalid_token' && location.origin + location.pathname != urlHome) {
						var param = "?redirect=" +window.location.href;

						window.location.href = urlHome + param;
						return;
					}

					self.alertar_erro(error.data.message);
				}
				else if (error.hasOwnProperty('message')) {
					console.log(error.message);
					//self.alertar_erro( "Not found..." );

				}				
				else {
					self.alertar_erro(error.data);
				}


				return error.data;
			});
		},

		export : (function() {
			var uri = 'data:application/vnd.ms-excel;base64,', 
			template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>', 
			base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }, 
			format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			return function(table, name) {
				if (!table.nodeType) table = document.querySelector(table);

				var dt = new Date();
				var day = dt.getDate();
				var month = dt.getMonth() + 1;
				var year = dt.getFullYear();
				var hour = dt.getHours();
				var mins = dt.getMinutes();
				var sec = dt.getMilliseconds();
				var postfix = year + "-" + month + "-" + day + "-" + hour + "-" + mins+"-"+sec;
				var ctx = {worksheet: name || 'tabela-'+postfix, table: table.innerHTML}

				var link1 = document.createElement("a");
				link1.download = ctx.worksheet+".xls";
				link1.href = uri + base64(format(template, ctx));
				link1.click();

				delete link1;

			}

		})()

	}

	return {
		solicitar : api.call,
		get : function(chamada, opts, callback){
			return api.call(chamada, 'GET', opts, callback);
		},
		post : function(chamada,opts, callback){
			return api.call(chamada, 'POST', opts, callback);
		},
		get_params_url: api.get_params_url,	
		alertar_sucesso: api.alertar_sucesso,
		alertar_erro: api.alertar_erro,
		loading: api.loading,
		urlApiBase: api.urlApiBase,
		urlAPI: api.urlAPI,
		export : api.export
	};

}).decorator("$xhrFactory", [
"$delegate", "$injector",
function($delegate, $injector) {
	return function(method, url) {
		var xhr = $delegate(method, url);
		var $http = $injector.get("$http");
		var callConfig = $http.pendingRequests[$http.pendingRequests.length - 1];
		if (angular.isFunction(callConfig.onProgress))
			xhr.addEventListener("progress", callConfig.onProgress);
		return xhr;
	};
}
]).directive('ngEnter', function () {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function (event) {
			if (event.which === 13) {
				scope.$apply(function () {
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
});
