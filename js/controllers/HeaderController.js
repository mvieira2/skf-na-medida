app.controller('Header', function($scope, $rootScope, $location,$timeout, API) {

	$scope.init_notificacoes = function () {
		API.post("notifications").then(function (r) {

			if (r) {
				$scope.notifications = r.data;
				
				if (!!!r.data['status']) {
					$scope.notifications_not_read = r.data.filter(function (n) {
						return n.is_read == 0;
					});
					console.log(r.data)
				}
			}

		});
	};

	$scope.read_notify = function (ID) {
		if (ID) {
			API.post("read_notification", { ID : ID }).then(function (r) {
				console.log(r.data);

				$scope.init_notificacoes();
			});
		}
	}


	$scope.init_notificacoes();

})