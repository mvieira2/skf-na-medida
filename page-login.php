<?php get_header('login'); ?>


<div ng-app="myApp" ng-controller="Login">
	<div id="loader" style="display: none;">
		<div class="fa-circle-o-notch fa-spin"></div>
	</div>
	<div class="main-login clearfix" ng-show="!isRegister">
		<div class="login-logo"></div>
		<div class="login-wrapper">
			<form name="formLogin" novalidate class="form" onsubmit="return;">
				<div class="login-warning" ng-show="erroLogin">Email ou Senha incorreta</div>
				<div class="input input-icon input-email">
					<input required type="text" ng-model="user.email" name="email" id="login-email" placeholder="E-mail" maxlength="100" />
				</div>
				<div class="input input-icon input-password">
					<input required type="password" ng-model="user.password" name="pass" id="login-pass" placeholder="Senha" maxlength="100" />
				</div>
				<div class="button button-submit">
					<input type="submit" name="submit" ng-click="logar(user, formLogin.$valid)" id="login-submit" value="Entrar" />
				</div>
			</form>
			<div class="login-info">
				<a href="#">Esqueci minha senha</a><br>
				<a href="#" ng-click="isRegister=!isRegister">Criar uma conta</a><br><br>
				<span>Alguns recursos deste site podem não ser compatíveis com o navegador Internet Explorer. É recomendado o acesso pelo Google Chrome.</span>
			</div>
		</div>
	</div>

	<div class="main-login clearfix" ng-show="isRegister">
		<div class="login-logo"></div>
		<div class="login-wrapper">
			<form class="form">
				<h1 class="register-title">Novo Cadastro</h1>

				<div class="login-warning">Email ou Senha incorreta</div>

				<h2 class="register-subtitle">Dados de acesso</h2>

				<div class="input input-icon input-email">
					<input type="text" name="email1" id="register-email1" placeholder="E-mail" maxlength="100" />
				</div>

				<div class="input input-icon input-password">
					<input type="password" name="pass1" id="login-pass1" class="login-input" placeholder="Senha" maxlength="100" />
				</div>

				<h2 class="register-subtitle">Informações pessoais</h2>

				<div class="input input-icon input-user">
					<input type="text" name="name" id="register-name" placeholder="Nome completo" maxlength="100" />
				</div>

				<div class="input input-icon input-id">
					<input type="text" name="id" id="register-id" placeholder="CPF" maxlength="100" />
				</div>

				<div class="input input-icon input-birth">
					<input type="text" name="birthdate" id="register-birthdate" class="datepicker" placeholder="Data de nascimento" maxlength="10" />
				</div>

				<h2 class="register-subtitle">Perfil de usuário</h2>

				<div class="select">
					<select name="group" id="register-group">
						<option disabled selected hidden>Selecione um Grupo</option>
						<option value="1">Grupo do João Carlos</option>
						<option value="2">Grupo do Silva Borges</option>
						<option value="3">Grupo do 678</option>
					</select>
				</div>

				<h2 class="register-subtitle">Endereço</h2>

				<div class="input">
					<input type="text" name="zipcode" id="register-zipcode" placeholder="CEP" maxlength="9" />
				</div>

				<div class="input input-icon input-home">
					<input type="text" name="address" id="register-address" placeholder="Endereço" maxlength="300" />
				</div>

				<div class="input">
					<input type="text" name="number" id="register-number" placeholder="Nº" maxlength="20" />
				</div>

				<div class="input">
					<input type="text" name="addressext" id="register-addressext" placeholder="Complemento" maxlength="100" />
				</div>

				<div class="input">
					<input type="text" name="neighborhood" id="register-neighborhood" placeholder="Bairro" maxlength="100" />
				</div>

				<div class="select">
					<!--<input type="text" name="state" id="register-state" placeholder="UF" maxlength="2" />-->
					<select name="state" id="register-state">
						<option disabled selected hidden>Selecione um Estado</option>
						<option value="AC">ACRE</option>
						<option value="AL">ALAGOAS</option>
						<option value="AP">AMAPÁ</option>
						<option value="AM">AMAZONAS</option>
						<option value="BA">BAHIA</option>
						<option value="CE">CEARÁ</option>
						<option value="DF">DISTRITO FEDERAL</option>
						<option value="ES">ESPÍRITO SANTO</option>
						<option value="GO">GOIÁS</option>
						<option value="MT">MATO GROSSO</option>
						<option value="MS">MATO GROSSO DO SUL</option>
						<option value="MG">MINAS GERAIS</option>
						<option value="PA">PARÁ</option>
						<option value="PB">PARAÍBA</option>
						<option value="PR">PARANÁ</option>
						<option value="PE">PERNAMBUCO</option>
						<option value="PI">PIAUÍ</option>
						<option value="RJ">RIO DE JANEIRO</option>
						<option value="RN">RIO GRANDE DO NORTE</option>
						<option value="RS">RIO GRANDE DO SUL</option>
						<option value="RO">RONDÔNIA</option>
						<option value="RR">RORAIMA</option>
						<option value="SC">SANTA CATARINA</option>
						<option value="SP">SÃO PAULO</option>
						<option value="SE">SERGIPE</option>
						<option value="TO">TOCANTINS</option>
					</select>
				</div>

				<div class="input">
					<input type="text" name="city" id="register-city" placeholder="Cidade" maxlength="100" />
				</div>

				<p class="register-terms">Ao enviar seu cadastro, você afirma que está aceitando o <a href="#">regulamento</a> e <a href="#">termos de uso</a> da campanha.</p>

				<div class="button button-submit">
					<input type="submit" name="submit" id="register-submit" value="Enviar cadastro!" />
				</div>

			</form>
			<div class="login-info">
				<ul>
					<li><a href="#">Suporte</a></li>
					<li><a href="#">Regulamento</a></li>
					<li><a href="#">Termos de uso</a></li>
					<li><a href="#">Política de Privacidade</a></li>
				</ul>
				<span>SKF na Medida © SKF do Brasil</span>
				<span>Alguns recursos deste site podem não ser compatíveis com o navegador Internet Explorer. É recomendado o acesso pelo Google Chrome.</span>
			</div>
		</div>
	</div>
</div>



<?php get_footer('login'); ?>