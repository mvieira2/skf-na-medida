<?php get_header(); ?>



<div ng-controller="Home" class="main clearfix">

	<input type="checkbox" id="modal-media" class="modal-viewer hidden-input" />
	<input type="checkbox" id="modal-tag" class="modal-viewer hidden-input" />
	<input type="checkbox" id="modal-report" class="modal-viewer hidden-input" />
	<input type="checkbox" id="modal-delete" class="modal-viewer hidden-input" />
	<input type="checkbox" id="modal-picture" class="modal-viewer hidden-input" />
	<aside class="aside perfil-mobile">

		<a href="#" class="user-block side-block clearfix">
			<div class="user-block-picture">
				<figure class="avatar">
					<img src="<?php echo get_template_directory_uri(); ?>/layout/images/default-avatar.png" />
				</figure>
			</div>
			<div class="user-block-info">
				<span id="userName" class="user-block-name">User</span>
				<span class="user-block-score">500 pontos</span>
			</div>
		</a>

		<h4 class="side-subtitle">Meu Grupo</h4>

		<a href="#" class="group-block side-block clearfix">
			<span class="group-block-name icon icon-group">Grupo Lorem Ipsum</span>
		</a>

		<h4 class="side-subtitle">Grupos</h4>

		<a href="#" class="group-block side-block clearfix">
			<span class="group-block-name icon icon-group">Grupo Lorem Ipsum</span>
		</a>

		<a href="#" class="group-block side-block clearfix">
			<span class="group-block-name icon icon-group">Grupo Lorem Ipsum</span>
		</a>

		<h4 class="side-subtitle">Ranking TOP 10</h4>

		<ul class="user-list">
			<li style="margin-bottom: 0px;">
				<div class="user-list-block clearfix">
					<a href="#" class="author-picture" style="cursor:default;">
						<figure class="avatar" for="modal-picture">
							<img src="http://rotativoskf.com.br/namedida/picture/avatar/MjE6NDE=41.jpg">
						</figure> 
					</a>
					<div class="author-info clearfix"><a href="#" class="author-name " style="cursor:default;">JOÃO JOÃO</a>
						<span class="author-points">8050 pontos</span>
					</div>
				</div>
			</li>
		</ul>

		<ul class="user-list">
			<li style="margin-bottom: 0px;">
				<div class="user-list-block clearfix">
					<a href="#" class="author-picture" style="cursor:default;">
						<figure class="avatar" for="modal-picture">
							<img src="http://rotativoskf.com.br/namedida/picture/avatar/MjE6NDE=41.jpg">
						</figure> 
					</a>
					<div class="author-info clearfix"><a href="#" class="author-name " style="cursor:default;">JOÃO JOÃO</a>
						<span class="author-points">8050 pontos</span>
					</div>
				</div>
			</li>
		</ul>

	</aside>

	<section class="timeline">

		<div class="edit-post container">
			<div class="edit-post-header">
				<ul class="clearfix">
					<li>
						<span class="icon icon-edit">Criar publicação</span>
					</li>
				</ul>
			</div>
			<div class="edit-post-content clearfix">
				<div class="edit-post-author">
					<figure class="avatar">
						<img src="<?php echo get_template_directory_uri(); ?>/layout/images/default-avatar.png" />
					</figure>
				</div>
				<textarea ng-model="newPost.content" class="edit-post-textarea" maxlength="500" placeholder="No que você está pensando?"></textarea>
				<button type="submit" class="button-blue button-right" ng-disabled="!newPost.content" ng-click="sendNewPost(newPost)"  >Enviar</button>


			</div>
			<div class="edit-post-options">
				<ul class="clearfix">
					<li>
						<label class="icon icon-picture" for="modal-picture" title="Enviar uma foto">Foto</label>
					</li>
					<li>
						<label class="icon icon-tag" for="modal-tag" title="Adicionar tags">Tags</label>
					</li>
					<li>
						<div id="mytags">

							<small ng-repeat="tag in newPost.tags track by tag.ID"> {{ tag.name || '' }}</small>
						</div>
					</li>
				</ul>
			</div>
			<div class="area-images-upload1 ng-cloak" ng-show="newPost.images.length">
				<div class="lists-imgs">
					<div ng-click="removeImage(i.ID)" ng-repeat="i in newPost.images track by $index" style="background: url('{{ i.image }}') no-repeat; "></div> 

				</div>
			</div>

		</div>

		<div ng-attr-id="{{'post_'+post.ID}}" class="post container ng-cloak" ng-repeat="post in posts track by post.ID">
			<div class="author clearfix">
				<a href="#" class="author-picture">
					<figure class="avatar">
						<img src="<?php echo get_template_directory_uri(); ?>/layout/images/default-avatar.png" />
					</figure>
				</a>
				<div class="author-info clearfix">
					<a href="#" class="author-name">{{ post.user_name || 'User Name' }}</a>
					<span class="author-date">{{ post.date_created }}</span>
				</div>
				<div class="author-options">
					<div class="author-options-report">
						<label for="modal-report" title="Deseja denunciar conteúdo impróprio?">Reportar publicação</label>
						<label for="modal-delete" title="Deseja excluir este post?">Excluir publicação</label>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="text">
					<p>{{ post.content }}

						<small ng-show="post.tags.length"> - </small>
						<small ng-repeat="tag in post.tags track by tag.ID">{{ tag.name }} </small>

					</p>

				</div>
				<div class="slider-imgs-posts slider-1">

					<div ng-repeat="image in post.images track by $index">
						<label class="media" for="modal-media">
							<figure class="figure" >
								<img class="img-view" width="664" height="374" ng-src="{{ image.image }}" />
							</figure>
						</label>					
					</div>
				</div>

			</div>
			<div class="interaction">
				<div class="interaction-status clearfix">
					<span class="interaction-status-likes" data-value="{{ post.count_likes }}"> curtida{{ post.count_likes == '1' ? '' : 's' }}</span>
					<span class="interaction-status-comments" data-value="{{ post.count_comments }}"> comentário{{ post.count_comments == '1' ? '' : 's' }}</span>
				</div>
				<div class="interaction-buttons clearfix">
					<label for="like-1" ng-click="like_post(post)" ng-class="{'liked' : post.liked > 0}" class="interaction-button interaction-buttons-likes" title="Curtir publicação"> Curtir </label>
					<label for="like-1" ng-click="focusComments('comment_post_'+post.ID)" ng-class="{'liked' : post.count_comments > 0}" class="interaction-button interaction-buttons-comments" title="Deixe um comentário..."> Comentar</label>
				</div>
			</div>
			<div class="comments">
				<div class="comments-container">
					<div class="comments-info">
					</div>
					<ul>
						<li ng-repeat="comment in post.comments track by comment.ID">
							<div class="author clearfix">
								<a href="#" class="author-picture">
									<figure class="avatar">
										<img src="<?php echo get_template_directory_uri(); ?>/layout/images/default-avatar.png" />
									</figure>
								</a>
								<div class="author-info clearfix">
									<a href="#" class="author-name">{{ comment.user_name }}</a>
									<div class="comments-content clearfix">
										<p ng-hide="comment.isEdit">{{ comment.content }}</p>

										<div ng-show="comment.isEdit" class="comments-edit-textarea clearfix" style="width: 68%; margin-bottom: 11px;" ng-show="comment.ID_user==current_user.ID_user">
										<textarea id="comment_field_{{comment.ID}}" ng-model="comment.content" ng-enter="updateComments(comment)" class="comments-textarea" maxlength="500" placeholder="Escreva um comentário...">www</textarea>
										<div class="comments-edit-options">

											<div ng-show="post.loadedComments" class="fa-circle-o-notch fa-spin ng-hide"></div>
											<button ng-hide="post.loadedComments" type="submit" class="comments-edit-send" ng-click="updateComments(comment)" title="Enviar comentário">
											</button>
										</div>
									</div>
									<div ng-show="comment.count_likes>0" class="comments-like-mark" data-value="{{ comment.count_likes }}"></div>
								</div>
							</div>
							<div class="comments-info ">
								<div>
									<a ng-click="like_comment(comment)" ng-class="{'liked' : comment.liked > 0}" class="comments-like">Curtir</a>
								</div>
								<div ng-show="comment.ID_user==current_user.ID_user">
									<span>·</span>
									<a ng-click="editCommentNow(comment);" class="comments-edit">Editar</a>
								</div>
								<div ng-show="comment.ID_user==current_user.ID_user">
									<span>·</span>
									<a ng-click="deleteComments(comment, post)" class="comments-remove">Excluir</a>
								</div>
								<div >
									<span>·</span>
									<span class="author-date">{{ comment.date_created }}</span>
									<span style="display: none;">Editado</span>
								</div>
							</div>
						</div>
					</li>
					<li ng-show="post.loadedComments1">
						<div class="load comments">
							<div  class="fa-circle-o-notch fa-spin"></div>
						</div>
					</li>

				</ul>
			</div>
			<div class="comments-edit">
				<div class="author clearfix">
					<a href="#" class="author-picture">
						<figure class="avatar">
							<img src="<?php echo get_template_directory_uri(); ?>/layout/images/default-avatar.png" />
						</figure>
					</a>
					<div class="comments-edit-textarea clearfix">
						<textarea id="comment_post_{{post.ID}}" ng-model="post.new_comments" ng-enter="sendComments(post)" class="comments-textarea" maxlength="500" placeholder="Escreva um comentário..."></textarea>
						<div class="comments-edit-options">
								<!-- <button type="submit" class="comments-img-send" title="Enviar imagem">
								</button> -->
								<div ng-show="post.loadedComments" class="fa-circle-o-notch fa-spin"></div>
								<button ng-hide="post.loadedComments" type="submit" class="comments-edit-send" ng-click="sendComments(post)" title="Enviar comentário">
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="button button-submit button-small" ng-show="posts && posts.length && postsPagination.page < postsPagination.total">
			<input type="button" name="submit" ng-click="loadPosts(postsPagination.page+1);" value="carregar mais">
		</div>

	</section>


	<div class="modal">
		<div class="modal-limit" >
			<div class="modal-container" style="width:100%;">
				<div id="modal-send-picture" class="modal-frame container">
					<div class="modal-header">
						<ul class="clearfix">
							<li><span class="icon icon-picture">Enviar foto</span></li>
							<li class="modal-close"><label class="modal-close-button" for="modal-picture"></label></li>
						</ul>
					</div>
					<div class="modal-content clearfix">

						<div class="area-file" ng-class="{'hasImages' : images_statics.length }">
							<span>{{ msgUpload || 'Arraste as imagens ou clique aqui' }}  </span>

							<input id="file" onchange="angular.element(this).scope().carregarImagens(angular.element(this))" type="file" accept="image/png, image/jpeg" multiple>
						</div>
						<div class="area-images-upload">
							<div class="lists-imgs">
								<div ng-click="removeImage(i.ID)" ng-repeat="i in images_statics track by $index" style="background: url('{{ i.image }}') no-repeat; "></div> 
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<div class="modal-close">
							<button type="submit" class="button-blue button-send" ng-click="uploadFileOk()">Adicionar</button>
						</div>
						
					</div>
				</div>
				<div id="modal-chose-tags" class="modal-frame container">
					<div class="modal-header">
						<ul class="clearfix">
							<li><span class="icon icon-tag">Adicionar Tags</span></li>
							<li class="modal-close"><label class="modal-close-button" for="modal-tag"></label></li>
						</ul>
					</div>
					<div class="modal-content clearfix">

						<div class="tags" ng-repeat="category in tags track by category.ID">
							<h4>{{ category.name }}</h4>
							<div class="med-6" ng-repeat="tag in category.tags track by tag.ID">
								<div >
									<div class="control-group">
										<label class="control control-checkbox">{{ tag.name }}
											<input ng-model="tag.selected" ng-checked="tag.selected" name="tags_selecionadas[]" type="checkbox" ng-id="tag_ID_{{tag.ID}}"  style="display:none">
											<div class="control_indicator">
											</div>
										</label>
									</div>
								</div>
								
							</div>
							<hr>
						</div>


					</div>
					<div class="modal-footer">
						<button type="submit" class="button-blue button-send" ng-click="addTagToNewPost()">Adicionar</button>
					</div>
				</div>
				<div id="modal-send-report" class="modal-frame container">
					<div class="modal-header">
						<ul class="clearfix">
							<li><span class="icon icon-report">Reportar publicação</span></li>
							<li class="modal-close"><label class="modal-close-button" for="modal-report"></label></li>
						</ul>
					</div>
					<div class="modal-content clearfix">

					</div>
					<div class="modal-footer">
						<button type="submit" class="button-blue button-send">Enviar</button>
					</div>
				</div>
				<div id="modal-delete-post" class="modal-frame container"> <div class="modal-header">
					<ul class="clearfix">
						<li><span class="icon icon-trash">Excluir publicação</span></li>
						<li class="modal-close"><label class="modal-close-button" for="modal-delete"></label></li>
					</ul>
				</div>
				<div class="modal-content clearfix">
					<p class="message-delete">Deseja realmente excluir esta publicação?</p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="button-blue button-send">Excluir</button>
				</div>
			</div>
		</div>
		<label class="modal-zoom-media" for="modal-media"> 
			<img class="img-view-model" />
		</label>
	</div>
</div>


</div>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controllers/HomeController.js?v=<?php echo get_rand(); ?>">
</script>

<?php get_footer(); ?>